Recommend virtualenv p3d_env. If you use a different env folder, make sure to add it to the .gitignore.

virtualenv setup:
1) simplejson
2) jsontemplate
3) twisted
4) autobahn
5) peewee
6) pyglet
