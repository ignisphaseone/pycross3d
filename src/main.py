import pygletreactor
pygletreactor.install()

import pyglet
from twisted.internet import reactor


def main():
    window = pyglet.window.Window()
    label = pyglet.text.Label('Game Over!',
                              font_name='Source Code Pro',
                              font_size=20,
                              x=window.width // 2, y=window.height // 2,
                              anchor_x='center', anchor_y='center')

    @window.event
    def on_draw():
        window.clear()
        label.draw()

    reactor.run()

if __name__ == '__main__':
    main()
